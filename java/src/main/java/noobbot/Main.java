package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.List;

import noobbot.message.in.ReceiveMsg;
import noobbot.message.in.impl.Car;
import noobbot.message.in.impl.CarPosition;
import noobbot.message.in.impl.Game;
import noobbot.message.out.impl.BotId;
import noobbot.message.out.impl.CreateRace;
import noobbot.message.out.impl.Direction;
import noobbot.message.out.impl.Join;
import noobbot.message.out.impl.JoinRace;
import noobbot.message.out.impl.Ping;
import noobbot.message.out.impl.SwitchLane;
import noobbot.message.out.impl.Throttle;
import noobbot.utils.Bend;
import noobbot.utils.CarUtils;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class Main
{

    private final Gson gson = new Gson();

    private String carName = "ricchioniAlVolante";

    @SuppressWarnings("unused")
    private String carColor = "verdePisello";

    private Bend prossimaCurva;

    private Game game;

    private double actualThrottleValue = 0.635;


    public static void main(String... args)
    {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        Socket socket;
        try
        {
            socket = new Socket(host, port);
            final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

            final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

            BotId botId = new BotId(botName, botKey);

            Join join = new Join(botId);
            CreateRace createRace = new CreateRace(botId, "keimola", "password", 1);
            JoinRace joinRace = new JoinRace(botId, "keimola", "password", 1);

            new Main(reader, writer, join, null, null);
        }
        catch (Throwable e)
        {
            System.out.println(e);
        }
    }

    public Main(final BufferedReader reader, PrintWriter writer, final Join join, final CreateRace createRace,
            final JoinRace joinRace) throws IOException
    {
        // RuleBase ruleBase = BrainUtils.initialiseDrools();
        // WorkingMemory knowledgeSession = BrainUtils.initializeMessageObjects(ruleBase);
        // knowledgeSession.setGlobal("writer", writer);

        String line = null;

        if (join != null)
        {
            CarUtils.send(writer, join);
        }
        else
        {
            // parte tutta da testare...
            CarUtils.send(writer, joinRace);
            CarUtils.send(writer, createRace);
        }

        while ((line = reader.readLine()) != null)
        {
            final ReceiveMsg<?> msgFromServer = gson.fromJson(line, ReceiveMsg.class);

            if (msgFromServer.getMsgType().equals("carPositions"))
            {
                @SuppressWarnings("serial")
                Type listType = new TypeToken<List<CarPosition>>() {}.getType();
                List<CarPosition> listCarPositions = new Gson().fromJson(msgFromServer.getData().toString(), listType);

                CarPosition myCarPosition = CarUtils.getCarPosition(listCarPositions, carName);

                // "telemetrie" solo per il primo giro altrimenti la syso sbrodola troppo...
                if (myCarPosition.getPiecePosition().getLap() == 0)
                {
                    System.out.println(myCarPosition);
                }

                Bend curva =
                        CarUtils.getNextBend(game.getRace().getTrack().getPieces(), myCarPosition.getPiecePosition());
                if (curva != null)
                {
                    if (!curva.equals(prossimaCurva))
                    {
                        System.out.println("prossima curva [" + curva + "]");

                        if (curva.getDirection().equals(Direction.Left))
                        {
                            CarUtils.send(writer, new SwitchLane(Direction.Left));
                        }
                        else
                        {
                            CarUtils.send(writer, new SwitchLane(Direction.Right));
                        }
                        // knowledgeSession.insert(curva);
                        // knowledgeSession.fireAllRules();

                        prossimaCurva = curva;
                    }
                }

                // lavorare qui!!! ...ovvero...in prossimità di curva frenare...altrimenti
                // accelerare
                // io farei i ragionamenti non solo sulla "prossimaCurva", da cui posso estrarre il
                // raggio
                // di curvatura, ma anche sull'angolo di assetto della macchina.
                // Sulla base della velocità attuale dell'auto (in base a tick posso calcolarmela
                // sapendo che un tick
                // del server equivale ad un secondo e sapendo anche la lunghezza di ogni segmento
                // di pista) e
                // della risposta alla derapata in curva posso calcolare il coefficente di attrito
                // della pista
                // che puo' essere diverso per ogni pista...insomma per calcolare la velocità
                // adeguata si possono
                // fare diversi ragionamenti sempre piu' sottili per modellizzare al meglio il
                // sistema...
                // Buon lavoro...io adesso devo andare a cambiare Matteo che si è scagazzato
                // tutto!!! Ciaooo ;-)
                if (myCarPosition.getPiecePosition().getPieceIndex() != prossimaCurva.getPieceIndex() - 1)
                {
                    actualThrottleValue = 0.63; // calcolare la massima acceleratore consentita
                    System.out.println("Acceleratore attuale [" + actualThrottleValue + "]");
                    CarUtils.send(writer, new Throttle(actualThrottleValue));
                }
                else
                {
                    actualThrottleValue = 0.63; // calcolare la velocità per cui non vado fuori
                                                // pista
                    System.out.println("Acceleratore attuale [" + actualThrottleValue + "]");
                    CarUtils.send(writer, new Throttle(actualThrottleValue));
                }
            }
            else if (msgFromServer.getMsgType().equals("join"))
            {
                System.out.println("Joined");
            }
            else if (msgFromServer.getMsgType().equals("joinRace"))
            {
                System.out.println("Race joined");
            }
            else if (msgFromServer.getMsgType().equals("gameInit"))
            {
                System.out.println("Race init");

                @SuppressWarnings("serial")
                Type type = new TypeToken<Game>() {}.getType();
                game = new Gson().fromJson(msgFromServer.getData().toString(), type);
            }
            else if (msgFromServer.getMsgType().equals("gameEnd"))
            {
                System.out.println("Race end");
            }
            else if (msgFromServer.getMsgType().equals("gameStart"))
            {
                System.out.println("Race start");
            }
            else if (msgFromServer.getMsgType().equals("yourCar"))
            {
                System.out.println("My car configuration");

                @SuppressWarnings("serial")
                Type listType = new TypeToken<Car>() {}.getType();
                Car car = new Gson().fromJson(msgFromServer.getData().toString(), listType);

                carName = car.getName();
                carColor = car.getColor();

                System.out.println("Il mio bot è [" + car + "]");
            }
            else
            {
                CarUtils.send(writer, new Ping());
            }
        }
    }

}
