package noobbot.message.out.impl;

import noobbot.message.out.SendMsg;

/**
 * {"msgType": "switchLane", "data": "Left"}
 * 
 * @author bigmoby
 *
 */
public class SwitchLane extends SendMsg
{

    private Direction value;
    
    public SwitchLane(Direction value)
    {
        this.value = value;
    }
    
    @Override
    protected Object msgData()
    {
        return value;
    }
    
    @Override
    protected String msgType()
    {
        return "switchLane";
    }

}
