package noobbot.message.out.impl;

import noobbot.message.out.SendMsg;


public class Ping extends SendMsg
{

    @Override
    protected String msgType()
    {
        return "ping";
    }
}
