package noobbot.message.out.impl;

import noobbot.message.out.SendMsg;


public class Turbo extends SendMsg
{
    private String message;

    public Turbo(String message)
    {
        this.message = message;
    }

    @Override
    protected Object msgData()
    {
        return message;
    }

    @Override
    protected String msgType()
    {
        return "turbo";
    }
}
