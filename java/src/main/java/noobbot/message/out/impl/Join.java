package noobbot.message.out.impl;

import noobbot.message.out.SendMsg;


/**
 * 
{"msgType": "join", "data": {
  "name": "Schumacher",
  "key": "UEWJBVNHDS"
}}
 *
 * @author bigmoby
 *
 */
public class Join extends SendMsg
{

    public final String name;

    public final String key;

    public Join(final BotId botId)
    {
        this.name = botId.getName();
        this.key = botId.getKey();
    }
    
    @Override
    protected String msgType()
    {
        return "join";
    }
}
