package noobbot.message.out.impl;

import noobbot.message.out.SendMsg;


/**
 * 
{"msgType": "createRace", "data": {
  "botId": {
    "name": "schumacher",
    "key": "UEWJBVNHDS"
  },
  "trackName": "hockenheim",
  "password": "schumi4ever",
  "carCount": 3
}}
 *
 * @author bigmoby
 *
 */
public class CreateRace extends SendMsg
{

    public final BotId botId;

    public final String trackName;

    public final String password;

    public final int carCount;

    public CreateRace(final BotId botId, final String trackName, final String password, final int carCount)
    {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType()
    {
        return "createRace";
    }

}
