package noobbot.message.out.impl;

import noobbot.message.out.SendMsg;

/**
 * 
{"msgType": "joinRace", "data": {
  "botId": {
    "name": "keke",
    "key": "IVMNERKWEW"
  },
  "trackName": "hockenheim",
  "password": "schumi4ever",
  "carCount": 3
}}
 *
 * @author bigmoby
 *
 */
public class JoinRace extends SendMsg
{

    public final BotId botId;

    public final String trackName;

    public final String password;

    public final int carCount;

    public JoinRace(final BotId botId, final String trackName, final String password, final int carCount)
    {
        this.botId = botId;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    @Override
    protected String msgType()
    {
        return "joinRace";
    }

}
