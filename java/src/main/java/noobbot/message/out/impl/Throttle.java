package noobbot.message.out.impl;

import noobbot.message.out.SendMsg;



public class Throttle extends SendMsg
{

    private double value;

    /**
     * Valori consentiti min = 0.0; max = 1.0
     * 
     * @author bigmoby
     *
     */
    public Throttle(double value)
    {
        this.value = value;
    }

    @Override
    protected Object msgData()
    {
        return value;
    }

    @Override
    protected String msgType()
    {
        return "throttle";
    }
}
