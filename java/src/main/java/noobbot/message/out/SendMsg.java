package noobbot.message.out;

import com.google.gson.Gson;


public abstract class SendMsg
{

    public String toJson()
    {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData()
    {
        return this;
    }

    protected abstract String msgType();
    
    class MsgWrapper
    {

        public final String msgType;

        public final Object data;

        MsgWrapper(final String msgType, final Object data)
        {
            this.msgType = msgType;
            this.data = data;
        }

        public MsgWrapper(final SendMsg sendMsg)
        {
            this(sendMsg.msgType(), sendMsg.msgData());
        }
    }

}
