package noobbot.message.in.impl;

import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * The gameInit message describes the racing track, the current racing session and the cars on
 * track. The track is described as an array of pieces that may be either Straights or Bends.
 * 
 * {
   "race": {
      "track": {
         "id": "keimola",
         "name": "Keimola",
         "pieces": [
            {
               "length": 100
            },
            {
               "length": 100
            },
            {
               "length": 100
            },
            {
               "length": 100,
               "switch": true
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 200,
               "angle": 22.5,
               "switch": true
            },
            {
               "length": 100
            },
            {
               "length": 100
            },
            {
               "radius": 200,
               "angle": -22.5
            },
            {
               "length": 100
            },
            {
               "length": 100,
               "switch": true
            },
            {
               "radius": 100,
               "angle": -45
            },
            {
               "radius": 100,
               "angle": -45
            },
            {
               "radius": 100,
               "angle": -45
            },
            {
               "radius": 100,
               "angle": -45
            },
            {
               "length": 100,
               "switch": true
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 200,
               "angle": 22.5
            },
            {
               "radius": 200,
               "angle": -22.5
            },
            {
               "length": 100,
               "switch": true
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "length": 62
            },
            {
               "radius": 100,
               "angle": -45,
               "switch": true
            },
            {
               "radius": 100,
               "angle": -45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "radius": 100,
               "angle": 45
            },
            {
               "length": 100,
               "switch": true
            },
            {
               "length": 100
            },
            {
               "length": 100
            },
            {
               "length": 100
            },
            {
               "length": 90
            }
         ],
         "lanes": [
            {
               "distanceFromCenter": -10,
               "index": 0
            },
            {
               "distanceFromCenter": 10,
               "index": 1
            }
         ],
         "startingPoint": {
            "position": {
               "x": -300,
               "y": -44
            },
            "angle": 90
         }
      },
      "cars": [
         {
            "id": {
               "name": "Ricky1",
               "color": "red"
            },
            "dimensions": {
               "length": 40,
               "width": 20,
               "guideFlagPosition": 10
            }
         }
      ],
      "raceSession": {
         "laps": 3,
         "maxLapTimeMs": 60000,
         "quickRace": true
      }
   }
}
 * 
 * 
 * 
 * 
 * 
 * @author bigmoby
 * 
 */
public class Game
{

    private Race race;



    @Override
    public String toString()
    {
        return "Game [race=" + race + "]";
    }


    public Race getRace()
    {
        return race;
    }


    public void setRace(Race race)
    {
        this.race = race;
    }

    public class Race
    {

        private Track track;

        private List<Cars> cars;

        private RaceSession raceSession;



        @Override
        public String toString()
        {
            return "Race [track=" + track + ", cars=" + cars + ", raceSession=" + raceSession + "]";
        }



        public RaceSession getRaceSession()
        {
            return raceSession;
        }



        public void setRaceSession(RaceSession raceSession)
        {
            this.raceSession = raceSession;
        }

        public class RaceSession
        {

            private int laps;

            private long maxLapTimeMs;

            private boolean quickRace;



            @Override
            public String toString()
            {
                return "RaceSession [laps=" + laps + ", maxLapTimeMs=" + maxLapTimeMs + ", quickRace=" + quickRace
                        + "]";
            }

            public int getLaps()
            {
                return laps;
            }

            public void setLaps(int laps)
            {
                this.laps = laps;
            }

            public long getMaxLapTimeMs()
            {
                return maxLapTimeMs;
            }

            public void setMaxLapTimeMs(long maxLapTimeMs)
            {
                this.maxLapTimeMs = maxLapTimeMs;
            }

            public boolean isQuickRace()
            {
                return quickRace;
            }

            public void setQuickRace(boolean quickRace)
            {
                this.quickRace = quickRace;
            }


        }

        public List<Cars> getCars()
        {
            return cars;
        }


        public void setCars(List<Cars> cars)
        {
            this.cars = cars;
        }

        public class Cars
        {

            private Car id;

            private Dimensions dimensions;



            @Override
            public String toString()
            {
                return "Cars [id=" + id + ", dimensions=" + dimensions + "]";
            }



            public Car getId()
            {
                return id;
            }



            public void setId(Car id)
            {
                this.id = id;
            }



            public Dimensions getDimensions()
            {
                return dimensions;
            }



            public void setDimensions(Dimensions dimensions)
            {
                this.dimensions = dimensions;
            }



            public class Dimensions
            {

                private double length;

                private double width;

                private double guideFlagPosition;



                @Override
                public String toString()
                {
                    return "Dimensions [length=" + length + ", width=" + width + ", guideFlagPosition="
                            + guideFlagPosition + "]";
                }

                public double getLength()
                {
                    return length;
                }

                public void setLength(double length)
                {
                    this.length = length;
                }

                public double getWidth()
                {
                    return width;
                }

                public void setWidth(double width)
                {
                    this.width = width;
                }

                public double getGuideFlagPosition()
                {
                    return guideFlagPosition;
                }

                public void setGuideFlagPosition(double guideFlagPosition)
                {
                    this.guideFlagPosition = guideFlagPosition;
                }


            }

        }


        public Track getTrack()
        {
            return track;
        }

        public void setTrack(Track track)
        {
            this.track = track;
        }

        public class Track
        {

            private String id;

            private String name;

            private List<Pieces> pieces;

            private List<Lane> lanes;

            private StartingPoint startingPoint;



            @Override
            public String toString()
            {
                return "Track [id=" + id + ", name=" + name + ", pieces=" + pieces + ", lanes=" + lanes
                        + ", startingPoint=" + startingPoint + "]";
            }



            public StartingPoint getStartingPoint()
            {
                return startingPoint;
            }



            public void setStartingPoint(StartingPoint startingPoint)
            {
                this.startingPoint = startingPoint;
            }

            public class StartingPoint
            {

                private double x;

                private double y;



                @Override
                public String toString()
                {
                    return "StartingPoint [x=" + x + ", y=" + y + "]";
                }

                public double getX()
                {
                    return x;
                }

                public void setX(double x)
                {
                    this.x = x;
                }

                public double getY()
                {
                    return y;
                }

                public void setY(double y)
                {
                    this.y = y;
                }

            }

            public List<Lane> getLanes()
            {
                return lanes;
            }


            public void setLanes(List<Lane> lanes)
            {
                this.lanes = lanes;
            }

            public String getId()
            {
                return id;
            }

            public void setId(String id)
            {
                this.id = id;
            }

            public String getName()
            {
                return name;
            }

            public void setName(String name)
            {
                this.name = name;
            }

            public List<Pieces> getPieces()
            {
                return pieces;
            }

            public void setPieces(List<Pieces> pieces)
            {
                this.pieces = pieces;
            }

            public class Lane
            {

                private double distanceFromCenter;

                private int index;



                @Override
                public String toString()
                {
                    return "Lane [distanceFromCenter=" + distanceFromCenter + ", index=" + index + "]";
                }

                public double getDistanceFromCenter()
                {
                    return distanceFromCenter;
                }

                public void setDistanceFromCenter(double distanceFromCenter)
                {
                    this.distanceFromCenter = distanceFromCenter;
                }

                public int getIndex()
                {
                    return index;
                }

                public void setIndex(int index)
                {
                    this.index = index;
                }
            }

            public class Pieces
            {

                private double length;

                private Switch _switch;

                private double angle;
                
                private int radius;

                
                @Override
                public String toString()
                {
                    return "Pieces [length=" + length + ", _switch=" + _switch + ", angle=" + angle + ", radius="
                            + radius + "]";
                }


                public int getRadius()
                {
                    return radius;
                }


                
                public void setRadius(int radius)
                {
                    this.radius = radius;
                }


                public class Switch
                {

                    @SerializedName("switch")
                    private boolean isSwitch;



                    @Override
                    public String toString()
                    {
                        return "Switch [isSwitch=" + isSwitch + "]";
                    }


                    public boolean isSwitch()
                    {
                        return isSwitch;
                    }


                    public void setSwitch(boolean isSwitch)
                    {
                        this.isSwitch = isSwitch;
                    }


                }


                public double getLength()
                {
                    return length;
                }


                public void setLength(double length)
                {
                    this.length = length;
                }



                public Switch get_switch()
                {
                    return _switch;
                }



                public void set_switch(Switch _switch)
                {
                    this._switch = _switch;
                }


                public double getAngle()
                {
                    return angle;
                }


                public void setAngle(double angle)
                {
                    this.angle = angle;
                }

            }
        }

    }


}
