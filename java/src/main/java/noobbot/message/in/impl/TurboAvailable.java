package noobbot.message.in.impl;


/**
 * {"msgType": "turboAvailable", "data": { "turboDurationMilliseconds": 500.0, "turboDurationTicks":
 * 30, "turboFactor": 3.0 }, "gameTick": 123}
 * 
 * @author bigmoby
 * 
 */
public class TurboAvailable
{

    private double turboDurationMilliseconds;

    private int turboDurationTicks;

    private double turboFactor;

    
    public double getTurboDurationMilliseconds()
    {
        return turboDurationMilliseconds;
    }

    
    public void setTurboDurationMilliseconds(double turboDurationMilliseconds)
    {
        this.turboDurationMilliseconds = turboDurationMilliseconds;
    }

    
    public int getTurboDurationTicks()
    {
        return turboDurationTicks;
    }

    
    public void setTurboDurationTicks(int turboDurationTicks)
    {
        this.turboDurationTicks = turboDurationTicks;
    }

    
    public double getTurboFactor()
    {
        return turboFactor;
    }

    
    public void setTurboFactor(double turboFactor)
    {
        this.turboFactor = turboFactor;
    }


    @Override
    public String toString()
    {
        return "TurboAvailable [turboDurationMilliseconds=" + turboDurationMilliseconds + ", turboDurationTicks="
                + turboDurationTicks + ", turboFactor=" + turboFactor + "]";
    }
    
}
