package noobbot.message.in.impl;


/**
 * The carPositions message describes the position of each car on the track.
 * 
 * Ex:
 * 
 * {"msgType": "carPositions", "data": [ { "id": { "name": "Schumacher", "color": "red" }, "angle":
 * 0.0, "piecePosition": { "pieceIndex": 0, "inPieceDistance": 0.0, "lane": { "startLaneIndex": 0,
 * "endLaneIndex": 0 }, "lap": 0 } }, 
 * { "id": { "name": "Rosberg", "color": "blue" }, "angle": 45.0,
 * "piecePosition": { "pieceIndex": 0, "inPieceDistance": 20.0, "lane": { "startLaneIndex": 1,
 * "endLaneIndex": 1 }, "lap": 0 } } ], "gameId": "OIUHGERJWEOI", "gameTick": 0}
 * 
 * 
 * @author bigmoby
 * 
 */
public class CarPosition
{

    private Id id;

    private double angle;

    private PiecePosition piecePosition;

    @Override
    public String toString()
    {
        return "CarPosition [id=" + id + ", angle=" + angle + ", piecePosition=" + piecePosition + "]";
    }


    public Id getId()
    {
        return id;
    }


    public void setId(Id id)
    {
        this.id = id;
    }

    /**
     * The angle depicts the car's slip angle. Normally this is zero, but when you go to a bend fast
     * enough, the car's tail will start to drift. Naturally, there are limits to how much you can
     * drift without crashing out of the track.
     */
    public double getAngle()
    {
        return angle;
    }


    public void setAngle(double angle)
    {
        this.angle = angle;
    }


    public PiecePosition getPiecePosition()
    {
        return piecePosition;
    }


    public void setPiecePosition(PiecePosition piecePosition)
    {
        this.piecePosition = piecePosition;
    }

    public class Id
    {

        private String name;

        private String color;

        public String getName()
        {
            return name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public String getColor()
        {
            return color;
        }

        public void setColor(String color)
        {
            this.color = color;
        }

        @Override
        public String toString()
        {
            return "Id [name=" + name + ", color=" + color + "]";
        }



    }

    public class PiecePosition
    {


        private int pieceIndex;


        private double inPieceDistance;


        private Lane lane;


        private int lap;


        /**
         * pieceIndex - zero based index of the piece the car is on
         */
        public int getPieceIndex()
        {
            return pieceIndex;
        }

        public void setPieceIndex(int pieceIndex)
        {
            this.pieceIndex = pieceIndex;
        }

        /**
         * inPieceDistance - the distance the car's Guide Flag has traveled from the start of the
         * piece along the current lane
         */
        public double getInPieceDistance()
        {
            return inPieceDistance;
        }

        public void setInPieceDistance(double inPieceDistance)
        {
            this.inPieceDistance = inPieceDistance;
        }

        /**
         * lane - a pair of lane indices. Usually startLaneIndex and endLaneIndex are equal, but
         * they do differ when the car is currently switching lane
         */
        public Lane getLane()
        {
            return lane;
        }

        public void setLane(Lane lane)
        {
            this.lane = lane;
        }


        /**
         * lap - the number of laps the car has completed. The number 0 indicates that the car is on
         * its first lap. The number -1 indicates that it has not yet crossed the start line to
         * begin it's first lap.
         */
        public int getLap()
        {
            return lap;
        }


        public void setLap(int lap)
        {
            this.lap = lap;
        }



        @Override
        public String toString()
        {
            return "PiecePosition [pieceIndex=" + pieceIndex + ", inPieceDistance=" + inPieceDistance + ", lane="
                    + lane + ", lap=" + lap + "]";
        }



        public class Lane
        {

            private int startLaneIndex;

            private int endLaneIndex;

            public int getStartLaneIndex()
            {
                return startLaneIndex;
            }

            public void setStartLaneIndex(int startLaneIndex)
            {
                this.startLaneIndex = startLaneIndex;
            }

            public int getEndLaneIndex()
            {
                return endLaneIndex;
            }

            public void setEndLaneIndex(int endLaneIndex)
            {
                this.endLaneIndex = endLaneIndex;
            }

            @Override
            public String toString()
            {
                return "Lane [startLaneIndex=" + startLaneIndex + ", endLaneIndex=" + endLaneIndex + "]";
            }


        }

    }
}
