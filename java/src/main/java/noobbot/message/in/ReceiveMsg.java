package noobbot.message.in;


public class ReceiveMsg<T>
{

    private final String msgType;

    private final T data;

    public ReceiveMsg()
    {
        this.msgType = "";
        this.data = null;
    }

    public ReceiveMsg(final String msgType, final T data)
    {
        this.msgType = msgType;
        this.data = data;
    }

    public String getMsgType()
    {
        return msgType;
    }

    public T getData()
    {
        return data;
    }

}
