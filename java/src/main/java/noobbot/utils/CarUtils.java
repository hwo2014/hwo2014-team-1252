package noobbot.utils;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import noobbot.message.in.impl.CarPosition;
import noobbot.message.in.impl.CarPosition.PiecePosition;
import noobbot.message.in.impl.Game.Race.Track.Pieces;
import noobbot.message.out.SendMsg;
import noobbot.message.out.impl.Direction;


public class CarUtils
{

    public static void send(final PrintWriter writer, final SendMsg msg)
    {
        writer.println(msg.toJson());
        writer.flush();
    }

    public static CarPosition getCarPosition(List<CarPosition> listCarPositions, String teamName)
    {
        for (CarPosition carPositions : listCarPositions)
        {
            if (teamName.equalsIgnoreCase(carPositions.getId().getName()))
            {
                return carPositions;
            }
        }

        return null;
    }

    public static Bend getNextBend(List<Pieces> pieces, PiecePosition actualPiecePosition)
    {
        int i = 0;

        for (Pieces piece : pieces)
        {
            if (i > actualPiecePosition.getPieceIndex())
            {
                double angle = piece.getAngle();

                if (angle != 0)
                {
                    int radius = piece.getRadius();

                    if (angle < 0)
                    {
                        return new Bend(i, Direction.Left, angle, radius);
                    }

                    return new Bend(i, Direction.Right, angle, radius);
                }
            }

            i++;
        }

        return null;
    }

    public static List<Integer> getIniziRettilinei(List<Pieces> pieces, int lunghezzaMinima)
    {
        List<Integer> result = new ArrayList<>();

        int count = 0;

        for (int j = 0; j < pieces.toArray().length; j++)
        {
            Pieces piece = pieces.get(j);
            
            double angle = piece.getAngle();

            if (angle != 0) // se c'é una curva resetto il contatore e passo al pezzo successivo...
            {
                count = 0;
                continue;
            }
            else // altrimenti incremento il contatore
            {
                count++;
            }

            if (count >= lunghezzaMinima)
            {
                result.add(j);
                j = j + lunghezzaMinima;
            }
        }

        return result;
    }
}
