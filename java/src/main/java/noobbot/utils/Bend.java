package noobbot.utils;

import noobbot.message.out.impl.Direction;


public class Bend
{

    private int pieceIndex;

    private Direction direction;

    private double angle;

    private int radius;

    public Bend(int pieceIndex, Direction direction, double angle, int radius)
    {
        this.pieceIndex = pieceIndex;
        this.direction = direction;
        this.angle = angle;
        this.radius = radius;
    }

    public int getPieceIndex()
    {
        return pieceIndex;
    }

    public void setPieceIndex(int pieceIndex)
    {
        this.pieceIndex = pieceIndex;
    }

    public Direction getDirection()
    {
        return direction;
    }

    public void setDirection(Direction direction)
    {
        this.direction = direction;
    }


    public double getAngle()
    {
        return angle;
    }


    public void setAngle(double angle)
    {
        this.angle = angle;
    }


    public int getRadius()
    {
        return radius;
    }


    public void setRadius(int radius)
    {
        this.radius = radius;
    }

    @Override
    public String toString()
    {
        return "Bend [pieceIndex=" + pieceIndex + ", direction=" + direction + ", angle=" + angle + ", radius="
                + radius + "]";
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        long temp;
        temp = Double.doubleToLongBits(angle);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((direction == null) ? 0 : direction.hashCode());
        result = prime * result + pieceIndex;
        result = prime * result + radius;
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Bend other = (Bend) obj;
        if (Double.doubleToLongBits(angle) != Double.doubleToLongBits(other.angle))
            return false;
        if (direction != other.direction)
            return false;
        if (pieceIndex != other.pieceIndex)
            return false;
        if (radius != other.radius)
            return false;
        return true;
    }



}
